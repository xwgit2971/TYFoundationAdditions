Pod::Spec.new do |s|
  s.name = 'TYFoundationAdditions'
  s.version = '0.0.5'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = 'Additional categories for the Foundation framework'
  s.homepage = 'https://gitlab.com/xwgit2971/TYFoundationAdditions'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYFoundationAdditions.git', :tag => s.version }
  s.source_files = 'TYFoundationAdditions/*.{h,m}'
  s.framework = 'Foundation', 'UIKit'
  s.requires_arc = true
end
