# TYFoundationAdditions
Additional categories for the Foundation framework

## How To Use

```objective-c
#import <TYFoundationAdditions/TYFoundationAdditions.h>
...
```

### 文件列表: 
* TYMacro.h
* NSString+TYAdditions.h
* NSArray+TYAdditions
* NSURL+TYAdditions.h
* NSTimer+TYAdditions.h
