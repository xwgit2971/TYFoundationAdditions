//
//  NSURL+TYAdditions.m
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/21.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "NSURL+TYAdditions.h"
#import <sys/xattr.h>
#import "TYMacros.h"

@implementation NSURL (TYAdditions)

+ (BOOL)ty_addSkipBackupAttributeToItemAtURL:(NSURL *)URL {
    if ([[NSFileManager defaultManager] fileExistsAtPath:[URL path]]) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.1")) {
            NSError *error = nil;
            BOOL success = [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
            if (error) {
                NSLog(@"addSkipBackupAttributeToItemAtURL: %@, error: %@", [URL lastPathComponent], error);
            }
            return success;
        }

        if (SYSTEM_VERSION_GREATER_THAN(@"5.0")) {
            const char* filePath = [[URL path] fileSystemRepresentation];
            const char* attrName = "com.apple.MobileBackup";
            u_int8_t attrValue = 1;
            int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
            return result == 0;
        }
    }
    return NO;
}

- (NSDictionary *)ty_queryParams {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *param in [[self query] componentsSeparatedByString:@"&"]) {
        NSArray *elts = [param componentsSeparatedByString:@"="];
        if([elts count] < 2) continue;
        [params setObject:elts[1] forKey:elts[0]];
    }
    return params;
}

@end
