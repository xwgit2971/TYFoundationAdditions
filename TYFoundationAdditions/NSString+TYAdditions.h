//
//  NSString+TYAdditions.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/29.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (TYAdditions)

// md5加密
- (NSString *)ty_md5;
// md5解密
- (NSString *)ty_stringFromMD5;
// SHA1加密
- (NSString*)ty_sha1Str;

// URL编码
- (NSString *)ty_URLEncoding;
// URL解码
- (NSString *)ty_URLDecoding;

// 去除空格
- (NSString *)ty_trimWhitespace;
// 是否空字符串
+ (BOOL)ty_isEmptyWithString:(NSString *)str;
// 是否非空字符串
+ (BOOL)ty_isNotEmptyWithString:(NSString *)str;
// 转NSNumber
- (NSNumber *)ty_convertToNumber;

// 判断是否为整形
- (BOOL)ty_isPureInt;
// 判断是否为浮点形
- (BOOL)ty_isPureFloat;

// 判断是否是手机号码
- (BOOL)ty_isPhoneNo;
// 判断是否是邮箱
- (BOOL)ty_isEmail;

- (BOOL)ty_isImage;
- (BOOL)ty_isMarkdown;

- (NSString *)ty_firstLetter;

// 判断是否包含Emoji表情
- (BOOL)ty_containsEmoji;

// 转换拼音
- (NSString *)ty_transformToPinyin;

// 左侧字符串尾部的空格
- (NSRange)ty_rangeByTrimmingLeftCharactersInSet:(NSCharacterSet *)characterSet;
// 右侧字符串头部的空格
- (NSRange)ty_rangeByTrimmingRightCharactersInSet:(NSCharacterSet *)characterSet;

// 左侧字符串
- (NSString *)ty_stringByTrimmingLeftCharactersInSet:(NSCharacterSet *)characterSet;
// 右侧字符串
- (NSString *)ty_stringByTrimmingRightCharactersInSet:(NSCharacterSet *)characterSet;

// 字符串Size
- (CGSize)ty_getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
// 字符串Height
- (CGFloat)ty_getHeightWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
// 字符串Width
- (CGFloat)ty_getWidthWithFont:(UIFont *)font constrainedToSize:(CGSize)size;

// Btye显示(bytes/KB/M/G)
+ (NSString *)ty_sizeDisplayWithByte:(CGFloat)sizeOfByte;

@end
