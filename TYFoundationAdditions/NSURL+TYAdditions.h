//
//  NSURL+TYAdditions.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/21.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (TYAdditions)

// 防止文件被备份到iCloud和iTunes
+ (BOOL)ty_addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
// URL参数({key: 参数名 value: 参数值})
- (NSDictionary *)ty_queryParams;

@end
