//
//  NSArray+TYAdditions.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 2016/11/9.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (TYAdditions)

// 数组元素倒序
- (NSArray *)ty_reverseSelf;

@end
