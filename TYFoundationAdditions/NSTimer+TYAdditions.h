//
//  NSTimer+TYAdditions.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 2016/10/24.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (TYAdditions)

// Timer初始化
+ (NSTimer *)ty_scheduledTimerWithTimeInterval:(NSTimeInterval)ti block:(void(^)())block repeats:(BOOL)yesOrNo;

// 继续
- (void)ty_resumeTimer;
// interval后继续
- (void)ty_resumeTimerAfterTimeInterval:(NSTimeInterval)interval;
// 暂停
- (void)ty_pauseTimer;

@end
