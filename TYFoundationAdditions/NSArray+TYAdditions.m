//
//  NSArray+TYAdditions.m
//  TYFoundationAdditions
//
//  Created by 夏伟 on 2016/11/9.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "NSArray+TYAdditions.h"

@implementation NSArray (TYAdditions)

- (NSArray *)ty_reverseSelf {
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:[self count]];
    NSEnumerator *reversedEnumerator = [self reverseObjectEnumerator];
    for (id element in reversedEnumerator) {
        [result addObject:element];
    }
    return result;
}

@end
