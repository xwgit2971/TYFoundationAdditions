//
//  TYMacros.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 2016/11/7.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#ifndef TYMacros_h
#define TYMacros_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// 版本比较
#define SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

// NSLog
#ifdef DEBUG
    #define NSLog(...) NSLog(__VA_ARGS__)
#else
    #define NSLog(...) {}
#endif

#define TYLogError(error) NSLog(@"Error: %@", error)

#endif /* TYMacros_h */
